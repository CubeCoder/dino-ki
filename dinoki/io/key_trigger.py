"""
This class implements a key trigger.

It can be used to simulate key events useful for our interaction with the game.

Example:
    kt = KeyTrigger()
    ...
    if should_jump:
        kt.trigger_up()
"""
# Import pynput library for Simulate key pressed
from pynput.keyboard import Key, Controller


class KeyTrigger(object):

    def __init__(self):
        print("Init KeyTrigger")
        # Init Controller from the library
        self.keyboard = Controller()

    def trigger_up(self):
        # simulate: press key up(Arrow Up)
        self.keyboard.press(Key.up)
        # simulate: release key up(Arrow Up)
        self.keyboard.release(Key.up)

    def trigger_down(self):
        # simulate: release key down(Arrow Down)
        self.keyboard.press(Key.down)
        # simulate: release key down(Arrow Down)
        self.keyboard.release(Key.down)


# Init kt as KeyTrigger class
kt = KeyTrigger()
# Execute trigger_up function from KeyTrigger(kt)
kt.trigger_up()
